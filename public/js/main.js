
var Main = Class({
    constructor: function () {
        this.data = [];
        this.key = 'close';

        this.data = [];

        this.actions = _.extend({}, Backbone.Events);

        this.amountCharts = $(".amount input:radio:checked")[0].value;
        this.typeRendering = $(".render input:radio:checked")[0].value;
        console.log('amountCharts', this.amountCharts);
        console.log('typeRendering', this.typeRendering);

        this.getData();
        this.createView();
    },

    createView: function () {

        d3.select('#charts').selectAll('*').remove();

        var params = {
            actions: this.actions,
            key: this.key,
            amountCharts: this.amountCharts
        };

        // this.view = new View(params);
        for (var i=0; i<this.amountCharts; i++){
            new View(params, i, this.typeRendering);
        }
    },

    getData: function () {
        var socket = io.connect();

        socket.on('news', function (data) {
            this.data.push(data);
            this.actions.trigger('addPoint', this.data);

            socket.emit('спасибо, получил');
        }.bind(this));

        window.onbeforeunload = function(e) {
            socket.disconnect();
        };
    },

    setAmountCharts: function () {
        this.actions.trigger('amountCharts', this.amountCharts);
    }
});

this.main = new Main();

function setAmount(radio) {
    console.log('radio', radio.value);
    this.main.amountCharts = radio.value;
    
    this.main.setAmountCharts();

    this.main.createView();

};

function setRendering(radio) {
    console.log('radio', radio.value);
    this.main.typeRendering = radio.value;

    this.main.createView();

};

