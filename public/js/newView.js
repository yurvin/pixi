var View = Class({

    constructor: function (params, index, typeRendering) {
        this.key = params.key;

        this.typeRendering = typeRendering;

        this.divClass = 'chart'+index;

        this.w = window.innerWidth*.8;
        this.h = window.innerHeight/params.amountCharts > 130 ? window.innerHeight/params.amountCharts : 130;

        this.dx = 2;
        this.x = 0;

        this.initChart();

        this.actions(params.actions);

    },

    actions: function (actions) {
        actions.on('addPoint', function (data) {
            this.data = data;
            this.lastPoint = data[data.length-1];

            this.domain = this.setDomain();

            this.callRequestAnimationFrame();

        }.bind(this));

        //click
        this.renderer.view.onclick = function (e) {

            var text = e.type + "; x "+e.layerX + "; y "+e.layerY;

            d3.select('.'+this.divClass).selectAll('span.eventClick').remove();
            d3.select('.'+this.divClass).selectAll('span.eventMousemove').remove();

            d3.select('.'+this.divClass)
                .append('span')
                .attr('class', 'eventClick')
                .text(text)
                .append('br');

            //pixiText
            this.stage.removeChild(this.textClick);

            this.textClick = new PIXI.Text(text, { font: '35px Snippet', fill: 'white', align: 'left' });
            this.textClick.position.set(20);


            this.stage.addChild(this.textClick);

        }.bind(this);

        //Mousemove
        this.renderer.view.onmousemove = function (e) {

            var text = e.type + "; x "+e.layerX + "; y "+e.layerY

            d3.select('.'+this.divClass).selectAll('span.eventMousemove').remove();
            d3.select('.'+this.divClass).selectAll('span.eventClick').remove();

            d3.select('.'+this.divClass)
                .append('span')
                .attr('class', 'eventMousemove')
                .text(text)
                .append('br');

            //pixiText
            this.stage.removeChild(this.textMousemove);

            this.textMousemove = new PIXI.Text(text, { font: '35px Snippet', fill: 'white', align: 'left' });
            this.textMousemove.position.set(20, 50);

            this.stage.addChild(this.textMousemove);

        }.bind(this);
    },

    initChart: function () {

        // this.renderer     = PIXI.autoDetectRenderer(this.w, this.h, null);
        // this.renderer     = new PIXI.CanvasRenderer(this.w, this.h, null, 1);

        this.renderer = new PIXI[this.typeRendering](this.w, this.h, null);

        this.stage        = new PIXI.Stage(0x2222, false);
        this.graphen      = new PIXI.Graphics();
        this.xdata        = [0];
        this.ydata        = [0];

        this.renderer.view.interactive = true;

        this.stage.addChild(this.graphen);

        this.graphen.position.x = this.w;
        this.graphen.position.y = this.h;

        var div = d3.select('#charts')
            .append('div')
            .attr('class', this.divClass);
        div.append('span');
        div.select('span').node().appendChild(this.renderer.view);

    },

    callRequestAnimationFrame: function () {
        if(!this.lastPoint) return;

        var animate = this.animate();
        requestAnimationFrame(animate);
    },

    setDomain: function () {

        var data = this.data;
        // var data = this.ydata.length >= 2 ? this.ydata: this.data;

        var min = d3.min(data, function (d) {
            return d[this.key];
        }.bind(this));

        var max = d3.max(data, function (d) {
            return d[this.key];
        }.bind(this));

        return {
            min: min,
            max: max,
            diff: max == min ? 1 : max - min
        };
    },

    animate: function animate() {

        return function animate() {

            var y = this.convertCoordinates(this.lastPoint[this.key]);
            y = y>this.h ? this.h : y;

            this.draw(this.dx, y);
            this.renderer.render(this.stage);

            // this.callRequestAnimationFrame();

        }.bind(this);
    },

    convertCoordinates: function (value) {
        return this.h * value / this.domain.diff;
    },
    
    draw: function (dx, y) {
        this.x += dx;
        this.xdata.push(this.x);
        this.ydata.push(-y);

        if (this.x > this.w){
            this.xdata.shift();
            this.ydata.shift();
        }
        this.graphen.clear();

        var n = this.xdata.length;

        for (var i = 0;i < n; i++){
            this.graphen.lineStyle(2, 0xff0000, 10);
            this.graphen.moveTo(this.xdata[i],this.ydata[i]);
            this.graphen.lineTo(this.xdata[i+1],this.ydata[i+1]);
        }
        this.graphen.position.x -= dx;

    }
});

