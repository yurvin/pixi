var w       = window.innerWidth;
var h       = window.innerHeight;

var dx     = 10;
var x      = 0;

var renderer     = PIXI.autoDetectRenderer(w, h, null);
var stage        = new PIXI.Stage(0x2222, false);
var	graphen      = new PIXI.Graphics();
var xdata        = [0];
var ydata        = [0];

document.body.appendChild(renderer.view);
stage.addChild(graphen);

graphen.position.x = w;
graphen.position.y = h;

requestAnimationFrame(animate);

function animate() {

    var y = Math.random() * h;
    console.log('y', y);
    draw(dx, y);
    renderer.render(stage);
    requestAnimationFrame(animate);
}

function draw(dx, y) {
    x += dx;
    xdata.push(x);
    ydata.push(-y);

    if (x>w){
        xdata.shift();
        ydata.shift();
    }
    graphen.clear();

    var n = xdata.length;

    for (var i = 0;i < n; i++){
        graphen.lineStyle(2, 0xff0000, 1);
        graphen.moveTo(xdata[i],ydata[i]);
        graphen.lineTo(xdata[i+1],ydata[i+1]);
    }
    graphen.position.x -= dx;
}


