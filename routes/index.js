var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/pixi', function (req, res, next) {
    res.render('pixi');
});

router.get('/three', function (req, res, next) {
    res.render('three');
});

module.exports = router;
